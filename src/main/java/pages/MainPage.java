package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MainPage {

    public WebDriver driver;
    LoginPage loginPage = new LoginPage(driver);

    public MainPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getHtmlLangAttribute() {
        return driver.findElement(By.tagName("html")).getAttribute("lang");
    }

    private WebElement getUserMenu() {
        return driver.findElement(By.xpath(".//*[@class = 'new-header-sub-menu']"));
    }

    private WebElement getLogOutButton() {
        return driver.findElement(By.xpath(".//*[@class = 'new-header-subs']/li/a[contains(@href,'/sign/out')]"));
    }

    public LoginPage doLogOut() {
        getUserMenu().click();
        getLogOutButton().click();
        return loginPage;
    }

}
