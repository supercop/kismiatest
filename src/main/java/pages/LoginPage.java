package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class LoginPage {

    public WebDriver driver;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
    }

    private WebElement getEmailField() {
        return driver.findElement(By.name("email"));
    }

    private WebElement getPassWordField() {
        return driver.findElement(By.name("password"));
    }

    private WebElement getLogInButton() {
        return driver.findElement(By.cssSelector("form.home-page-form.js_signInForm > a"));
    }

    private WebElement getChangeLanguageButton() {
        return driver.findElement(By.xpath(".//*[@class = 'home-page-languages__current js_showLanguagesList']"));
    }

    private WebElement getLanguageOption(String option) {
        return driver.findElement(By.xpath(".//*[@class = 'home-page-languages__list js_languagesList']/" +
                "li[contains(.,'" + option + "')]"));
    }

    public WebElement getSignInForm() {
        return driver.findElement(By.xpath(".//*[@class = 'home-page-form js_signInForm']"));
    }

    public MainPage logInWith(String logIn, String pass) {
        getEmailField().sendKeys(logIn);
        getPassWordField().sendKeys(pass);
        getLogInButton().click();
        return new MainPage(driver);
    }

    public LoginPage setSiteLanguage(String lang) {
        getChangeLanguageButton().click();
        getLanguageOption(lang).click();
        return this;
    }

    public boolean userIsOnLogInPage() {
        return getSignInForm().isDisplayed();
    }

}
