package tests;

import common.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.MainPage;

import static common.TestData.*;


public class LanguageTest extends BaseTest {

    private LoginPage loginPage;
    private MainPage mainPage;

    @Test
    public void languageTest() {
        loginPage = new LoginPage(driver);
        mainPage = new MainPage(driver);
        loginPage.setSiteLanguage(EN_LANG);
        Assert.assertEquals(mainPage.getHtmlLangAttribute(), EN_LANG);
        loginPage.logInWith(TEST_EMAIL, TEST_PASS);
        Assert.assertEquals(mainPage.getHtmlLangAttribute(), RU_LANG);
        mainPage.doLogOut();
        Assert.assertTrue(loginPage.userIsOnLogInPage());
    }
}
